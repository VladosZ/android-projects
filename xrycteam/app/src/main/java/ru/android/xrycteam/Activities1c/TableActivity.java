package ru.android.xrycteam.Activities1c;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import ru.android.xrycteam.R;

public class TableActivity extends AppCompatActivity {

    public TableLayout table;
    final public  int TEXT_SIZE = 18;
    public String TITLE_DESCRIPTION;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_scroll);

        // get data from 1C web-service (JSON);
        table = findViewById(R.id.tableLayout);

        table.setStretchAllColumns(true);
        table.setShrinkAllColumns(true);

        // add title and description
        setTitle("Заголовок");

        TITLE_DESCRIPTION = "Описание действий на странице";

        // create Header
        LinearLayout headers = createTableHeader(new String[] {"Заголовок 1", "Заголовок 2", "Заголовок 3"});
        table.addView(headers);
        // add data
        for(int i = 0; i < 30; i++)
        {
            TableRow row = new TableRow(this);
            TextView text = new TextView(this);
            EditText editText = new EditText(this);
            CheckBox checkBox = new CheckBox(this);

            text.setText("N# [" + i + "]");
            editText.setText("123123");
            editText.setMaxWidth(150);
            row.addView(text);
            row.addView(editText);
            row.addView(checkBox);

            table.addView(row);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.step_menu, menu);
        return  true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (item.getItemId())
        {
            case R.id.help_menu:
                createMessageDialog("Помощь", TITLE_DESCRIPTION);
                return true;
            case R.id.session_exit:
                createMessageDialog("Выход", "Вы действительно хотите выйти?");
            default:
                return super.onOptionsItemSelected(item);
        }

    }


    public void createMessageDialog(String title, String text)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(text)
                .setTitle(title);
        builder.setPositiveButton("OK", (dialog, id) -> {
            // User clicked OK button
        });
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public TableRow createTableHeader(String... columns) {

        TableRow headerHorizontal = new TableRow(this);

        for(int i = 0; i < columns.length; i++) {
            TextView columnTitle = new TextView(this);
            columnTitle.setTextSize(TEXT_SIZE);
            columnTitle.setText(columns[i]);
            columnTitle.setTextColor(Color.WHITE);
            columnTitle.setBackgroundColor(Color.LTGRAY);
            headerHorizontal.addView(columnTitle);
        }

        return headerHorizontal;
    }


}
