package ru.android.xrycteam;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class SendActivity extends AppCompatActivity {

    EditText text1, text2, text3, text4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);
        TextView codeText = findViewById(R.id.numberText);
        text1 = findViewById(R.id.textInput1);
        text2 = findViewById(R.id.textInput2);
        text3 = findViewById(R.id.textInput3);
        text4 = findViewById(R.id.textInput4);

        Button submit = findViewById(R.id.submitButton);

        Bundle args = getIntent().getExtras();
        if(args != null)
        {
            codeText.setText("Код: " + args.get("code").toString());
        }

        submit.setOnClickListener(v -> {

            AsyncSend send = new AsyncSend();
            send.execute(args.get("code").toString(),
                    text1.getText().toString(),
                    text2.getText().toString(),
                    text3.getText().toString(),
                    text4.getText().toString());
        });
    }
    public  void getResponse(String response)
    {
        if (response.equals("1"))
        {
            Toast.makeText(
                    getApplicationContext(),
                    "Успешно",
                    Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(
                    getApplicationContext(),
                    "Ошибка: " + response,
                    Toast.LENGTH_SHORT).show();
        }
        finish();


    }

    class AsyncSend extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        @Override
        protected String doInBackground(String... params) {

            HttpURLConnection client = null;
            String BASE_URL = "http://192.168.0.7/api.php";
            OutputStream out = null;
            try
            {
                URL url = new URL(BASE_URL);
                client = (HttpURLConnection) url.openConnection();
                client.setDoOutput(true);
                client.setRequestMethod("POST");
                client.setRequestProperty("Accept-Charset", "UTF-8");
                client.setReadTimeout(10000);
                client.setConnectTimeout(15000);
                client.connect();
                String json = "{\"method\":\"code\", " +
                        "\"code\":\"" + params[0]+ "\", " +
                        "\"text1\":\""+params[1]+"\", " +
                        "\"text2\":\""+params[2]+"\", " +
                        "\"text3\":\""+params[3]+"\", " +
                        "\"text4\":\""+params[4]+"\"}";
                out = new BufferedOutputStream(client.getOutputStream());
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                writer.write(json);
                writer.flush();
                writer.close();
                out.close();
                InputStream inputStream = new BufferedInputStream(client.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null)
                {
                    result.append(line);
                }
                return  result.toString();
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                return "-1";
            }
            finally {
                if(client != null) client.disconnect();
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            getResponse(s);
        }
    }

}
