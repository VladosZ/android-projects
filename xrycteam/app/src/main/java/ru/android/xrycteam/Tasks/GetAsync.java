package ru.android.xrycteam.Tasks;

import android.os.AsyncTask;
import android.util.Base64;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetAsync extends AsyncTask<String, Integer, String> {

    private CallBack callBack;
    private ProgressCallBack progressCallBack;

    public  GetAsync(final  CallBack callBack, final ProgressCallBack progressCallBack) {
        this.callBack = callBack;
        this.progressCallBack = progressCallBack;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(String... params) {
        HttpURLConnection client = null;
        String BASE_URL_GET = params[0];
        publishProgress(0);
        try
        {
            final String BASE_AUTH = "Basic " + Base64.encodeToString("odata.user:123852".getBytes(), Base64.NO_WRAP);
            URL url = new URL(BASE_URL_GET);
            client = (HttpURLConnection) url.openConnection();
            client.setDoOutput(false);
            client.setRequestMethod("GET");
            client.setRequestProperty("Accept-Charset", "UTF-8");
            client.setRequestProperty("Content-Type", "application/json");
            client.setRequestProperty("Authorization", BASE_AUTH);
            client.setReadTimeout(10000);
            client.setConnectTimeout(15000);
            client.setUseCaches(true);
            client.connect();
            InputStream inputStream = new BufferedInputStream(client.getInputStream());
            if(client.getResponseCode() == 200)
            {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null)
                {
                    result.append(line);
                }
                publishProgress(1);
                return  result.toString();
            }
            return "-1";
        }

        catch (IOException ex)
        {
            ex.printStackTrace();
            publishProgress(-1);
            return "ex: " + ex.toString();
        }

        finally {
            if(client != null) client.disconnect();
        }
    }

    @Override
    protected void onPostExecute(String aVoid) {
        super.onPostExecute(aVoid);
        if(callBack != null)
            callBack.callBack(aVoid);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if(progressCallBack != null)
            progressCallBack.onUpdate(values);
    }

    public  interface CallBack {
        void callBack(String result);
    }

    public interface ProgressCallBack {
        void onUpdate(Integer... values);
    }


}
