package ru.android.xrycteam;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collection;

import ru.android.xrycteam.Activities1c.TableActivity;

public  class MainActivity extends AppCompatActivity {

    boolean isDoubleBack = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView text = findViewById(R.id.mainText);
        Button rssScannerButton = findViewById(R.id.rssScaner);
        Button qrScannerButton = findViewById(R.id.qrScanner);
        Button settingsButton = findViewById(R.id.settings);
        Button photoButton = findViewById(R.id.photo);

        rssScannerButton.setOnClickListener(v -> {
            setScanner(IntentIntegrator.PRODUCT_CODE_TYPES);
        });

        qrScannerButton.setOnClickListener(v -> {
            setScanner(IntentIntegrator.QR_CODE_TYPES);
        });

        photoButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, TableActivity.class);
            startActivity(intent);
        });
    }

    public void setScanner(Collection<String> mode)
    {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(ScannerActivity.class);
        integrator.setDesiredBarcodeFormats(mode);
        integrator.setOrientationLocked(true);
        integrator.setPrompt("Сканирование");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(true);
        integrator.setBarcodeImageEnabled(false);
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null)
        {
            if(result.getContents() == null)
            {
                Toast.makeText(this, "Сканирование отменено", Toast.LENGTH_LONG).show();
            }
            else
            {
                //Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(this, SendActivity.class);
                intent.putExtra("code", result.getContents());
                startActivity(intent);
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        if(isDoubleBack)
        {
            super.onBackPressed();
            return;
        }

        isDoubleBack = true;
        Toast.makeText(getApplicationContext(), "Нажмите еще раз для выхода", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isDoubleBack = false;
            }
        }, 2000);
    }
}
