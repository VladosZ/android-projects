package ru.android.xrycteam;


import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.android.xrycteam.Activities1c.TableActivity;
import ru.android.xrycteam.Tasks.GetAsync;

// Подключаем класс с интерфейсами.
public class LoginActivity extends AppCompatActivity  implements GetAsync.CallBack, GetAsync.ProgressCallBack {

    private EditText password;
    private ProgressBar progressBar;
    private Spinner spinner;
    public Boolean isPasswordCorrect = false;
    public String METHOD = "GETUSERS"; // Метод получения данных, используется для обработки ответа JSON;
    GetAsync getAsyncClass = new GetAsync(this, this); // Создаём новый AsyncTask класс для GET-запроса.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Настройка активити
        setContentView(R.layout.activity_login_white);
        password = findViewById(R.id.passwordEntry);
        progressBar = findViewById(R.id.progressBar2);
        progressBar.setVisibility(ProgressBar.INVISIBLE);
        spinner = findViewById(R.id.spinner);
        Button button = findViewById(R.id.SignInButton);
        TextView networkButton = findViewById(R.id.settingsButton);

        // (Получаем список пользовтелей) В качестве данных указываем IP-адес с запросом.
        getAsyncClass.execute("http://93.170.103.88:40800/Temp/odata/standard.odata/Catalog_Пользователи?$format=json&$select=Description");

        // Обработчик нажатия на кнопку для смены IP-адреса;
        networkButton.setOnClickListener(v -> {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);
            alertDialog.setTitle("Изменение настроек сети");
            alertDialog.setMessage("Введите новый IP");
            final EditText networkIP = new EditText(LoginActivity.this);
            networkIP.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
            networkIP.setKeyListener(DigitsKeyListener.getInstance("0123456789."));
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            networkIP.setLayoutParams(lp);
            alertDialog.setView(networkIP);
            alertDialog.setPositiveButton("Сохранить", (dialog, which) -> {
               // String newIP = networkIP.getText().toString();
            });
            alertDialog.setNegativeButton("Отменить", ((dialog, which) -> dialog.cancel()));
            alertDialog.show();
        });

        // Обработчик нажатия на кнопку "Войти";
        button.setOnClickListener(v -> {
            if(spinner.getSelectedItemId() == 0) {
                Toast.makeText(getApplicationContext(), "Выберите пользователя", Toast.LENGTH_SHORT).show();
                return;
            }

            if(TextUtils.isEmpty(password.getText())) {
                Toast.makeText(getApplicationContext(), "Введите пароль", Toast.LENGTH_SHORT).show();
                return;
            }

            METHOD = "LOGIN"; // Метод для проверки пароля.
            String login = spinner.getSelectedItem().toString();
            String pass = password.getText().toString();
            if(!checkPassword(login, pass)) {
                Toast.makeText(getApplicationContext(), "Пароль неверный", Toast.LENGTH_SHORT).show();
                return;
            }

            // Если пароль верный установить METHOD в следующий этап.
            // GETDATA - получает данные о сессии пользователя.
            // String url = Запрос на получение сессии;
            METHOD = "GETDATA";
            // getAsyncClass.execute(url);

            // После того, как получим данные от getAsyncClass выполнится ф-я callback, которая передаст данные в ф-ю logIn;
            // Ф-я login разберет ответ и откроет нужную страницу, в которую передаст данные в формате JSON.
        });
    }

    // Функция разбирает ответ и загружает данные в спиннер. (Вытаскивает из JSON имена пользователей);
    public void setSpinner(String response)
    {
        try {
            JSONObject jsonResponse = new JSONObject(response);
            if(!jsonResponse.has("value"))
                return;

            JSONArray value = (JSONArray) jsonResponse.get("value");
            if(value.length() == 0)
                return;

            int length = value.length();
            String[] data = new String[length];

            for(int i = 0; i < length; i++)
            {
                JSONObject object = new JSONObject(value.getString(i));
                data[i] = object.get(object.names().getString(0)).toString();
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.spinner_list, data);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinner.setAdapter(adapter);
            spinner.setSelection(0);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    // Функция разбирает ответ и устанавливает флаг проверки пароля. (Вытаскивает из JSON Ref-Key пользователя, если пароль верный
    public void setIsPasswordCorrect(String request) {
        isPasswordCorrect = request.length() > 10;
    }


    //
    public boolean checkPassword(String login, String pass) {

        // Сформировать URL исходя из логина и пароля.
        String url = "http://93.170.103.88:40800/Temp/odata/standard.odata/Catalog_Пользователи?$format=json&$select=Description&$filer = ФИЛЬТР!!!!";
        // Создать запрос по IP;
        // getAsyncClass.execute(Где пользователь eq 'login' and пароль eq 'pass');
        // Выполнится GET-запрос к 1С; После получения данных ответ будет выполнена функция callBack, которая передаст
        // ответ в функцию setIsPasswordCorrect. Функция разберёт ответ и сделает переменную true, если пароль верный или false, если не верный.
        return isPasswordCorrect;

    }

    // Функция выполняется при успешной авторизации и передаёт полученные данные на следующий этап;
    public void logIn(String response) {
        Intent intent = new Intent(LoginActivity.this, TableActivity.class);
        intent.putExtra("JSON", response);
        startActivity(intent);
        finish();
    }

    // Интерфейс для onPostExecute (по завершению передачи данных);
    @Override
    public void callBack(String result) {

        switch (METHOD)
        {
            case "GETUSERS":
                setSpinner(result);
                break;
            case "LOGIN":
                setIsPasswordCorrect(result);
                break;
            case "GETDATA":
                logIn(result);
                break;
        }


    }

    // Интерфейс для onProgressUpdate (Прогресс передачи данных);
    @Override
    public void onUpdate(Integer... result) {
        switch (result[0])
        {
            // Начало передачи данных
            case 0:
                progressBar.setVisibility(ProgressBar.VISIBLE);
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                break;

            // Конец передачи данных
            case 1:
                progressBar.setVisibility(ProgressBar.INVISIBLE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                break;
        }

    }
}



