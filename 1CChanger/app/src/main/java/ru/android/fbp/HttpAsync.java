package ru.android.fbp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpAsync extends AsyncTask<String, Integer, String> {

    private CallBack callBack;
    private ProgressCallBack progressCallBack;
    private String URL_START; // IP:PORT/DbName/
    private String userPass;

    HttpAsync(final CallBack callBack, final ProgressCallBack progressCallBack, Context context) {
        this.callBack = callBack;
        this.progressCallBack = progressCallBack;
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String ip = sharedPref.getString("server_ip", "localhost");
        String port = sharedPref.getString("server_port", "8080");
        String db = sharedPref.getString("server_db", "Db_fin");
        userPass = sharedPref.getString("network_pass", "111");
        URL_START = "http://" + ip + ":" + port + "/" + db + "/odata/standard.odata/";
        Log.d("Debug", URL_START);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(String... params) {
        HttpURLConnection client = null;
        String BASE_URL_GET =  URL_START + params[0];
        publishProgress(0);
        try
        {
            // Аутентификация
            final String BASE_AUTH = "Basic " + Base64.encodeToString(("odata.user:"+userPass).getBytes(), Base64.NO_WRAP);

            // Создание нового запроса по URL
            URL url = new URL(BASE_URL_GET);
            client = (HttpURLConnection) url.openConnection();
            client.setRequestProperty("Accept-Charset", "UTF-8");
            client.setRequestProperty("Content-Type", "application/json");
            client.setRequestProperty("Authorization", BASE_AUTH);
            client.setReadTimeout(10000);
            client.setConnectTimeout(10000);
            client.setUseCaches(false);

            // PATCH OR GET Request
            if(params.length > 1)
            {
                Log.d("Debug", params[1]);
                client.setDoOutput(true);
                client.setRequestMethod("PATCH");
                String data = params[1];
                OutputStreamWriter streamWriter = new OutputStreamWriter(
                        client.getOutputStream());
                streamWriter.write(data);
                streamWriter.close();
            }
            else
            {
                client.setRequestMethod("GET");
                client.setDoOutput(false);
                client.connect();
            }

            // Output
            Log.d("Debug", client.getResponseCode() + " ");
            InputStream inputStream;
            if(client.getResponseCode() == HttpURLConnection.HTTP_OK)
            {

                inputStream = client.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null)
                {
                    result.append(line);
                }
                publishProgress(1);
                Log.d("Debug", result.toString());
                return  result.toString();
            }  else if(client.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                publishProgress(-2);
                return "Ошибка авторизации";
            } else {
                inputStream = client.getErrorStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null)
                {
                    result.append(line);
                }
                publishProgress(-1);
                return  result.toString();
            }






        }

        catch (IOException ex)
        {
            ex.printStackTrace();
            publishProgress(-1);
            return "ex: " + ex.toString();
        } finally {
            if(client != null) client.disconnect();
        }
    }

    @Override
    protected void onPostExecute(String aVoid) {
        super.onPostExecute(aVoid);
        if(callBack != null)
            callBack.callBack(aVoid);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if(progressCallBack != null)
            progressCallBack.onUpdate(values);
    }

    public  interface CallBack {
        void callBack(String result);
    }

    public interface ProgressCallBack {
        void onUpdate(Integer... values);
    }

}
