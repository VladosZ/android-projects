package ru.android.fbp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity implements HttpAsync.CallBack, HttpAsync.ProgressCallBack {

    // private UI - animations
    ImageView background, accessory;
    ConstraintLayout menu;
    Animation fromBottom;

    // private Buttons
    ImageButton sendAllButton, sendNullButton, sendCustomButton;
    RadioGroup radioGroup;

    public  Boolean IsFirst = true;
    public  String METHOD = "GET";
    private String currentGuid = "";

    private HttpAsync.CallBack callBack = this;
    private  HttpAsync.ProgressCallBack update = this;

    private AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        builder = new AlertDialog.Builder(this);
        sendAllButton = findViewById(R.id.send_all_button);
        sendNullButton = findViewById(R.id.send_null_button);
        sendCustomButton = findViewById(R.id.send_custom_button);

        radioGroup = findViewById(R.id.radioGroup);

        fromBottom = AnimationUtils.loadAnimation(this, R.anim.frombottom);
        background = findViewById(R.id.imageView);
        accessory = findViewById(R.id.imageAccessory);
        menu = findViewById(R.id.menu);
        menu.setVisibility(View.INVISIBLE);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        ImageButton settings = findViewById(R.id.settings_button);
        Log.d("Debug", settings.toString());
        View.OnClickListener settingsButton_Click = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Debug", "settingsClicked");
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivityForResult(intent, 1);
            }
        };

        sendAllButton.setOnClickListener(change_State);
        sendNullButton.setOnClickListener(change_State);
        sendCustomButton.setOnClickListener(change_State);
        settings.setOnClickListener(settingsButton_Click);
        radioGroup.setOnCheckedChangeListener(change_Status);

        String ip = sharedPref.getString("server_ip", "");
        String port = sharedPref.getString("server_port", "");
        String db_name = sharedPref.getString("server_db", "");
        String pass = sharedPref.getString("network_pass", "");

        if(TextUtils.isEmpty(ip)
                || TextUtils.isEmpty(port)
                || TextUtils.isEmpty(db_name)
                || TextUtils.isEmpty(pass))
        {
            StartAnimation();
            menu.setVisibility(View.INVISIBLE);
            CreateAlertDialog("Внимание!", "Необходимо указать параметры подключения в настройках.");
        } else {
            METHOD = "GET";
            HttpAsync GetAsync = new HttpAsync(callBack, update, getApplicationContext());
            GetAsync.execute("Catalog_КонтурEDI_ТочкиДоставкиWEB?&$filter=Code eq '000000001'&$format=json");
        }

    }

    // Задать режим и статус
    public  void SetState(String response) {
        try {
            JSONObject jsonResponse = new JSONObject(response);
            if(!jsonResponse.has("value")) {
                Log.d("Debug", "Нет данных");
                return;
            }

            JSONArray value = (JSONArray) jsonResponse.get("value");
            JSONObject values = (JSONObject) value.get(0);

            int state = values.getInt("Направление");
            String type = values.getString("ТипСообщения");
            String status = values.getString("Статус");

            currentGuid = values.getString("Ref_Key");
            if(type.length() > 1) {
                SetupButtons(state, type.split(";"), status);
            }

            if(IsFirst) {
                StartAnimation();
                IsFirst = false;
            }

        }
        catch (JSONException e)
        {
            Toast.makeText(getApplicationContext(), "Ошибка при получении данных. Проверьте сетевые настройки.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    // Обновить режим и статус
    public  void UpdateState(String response) {
        try {
            JSONObject jsonResponse = new JSONObject(response);
            if(!jsonResponse.has("Направление") ||
                    !jsonResponse.has("Статус") ||
                    !jsonResponse.has("ТипСообщения") ||
                    !jsonResponse.has("Ref_Key") ) {

                Log.d("Debug", "Нет данных");
                return;
            }

            int state = jsonResponse.getInt("Направление");
            String type = jsonResponse.getString("ТипСообщения");
            String status = jsonResponse.getString("Статус");

            currentGuid = jsonResponse.getString("Ref_Key");

            if(type.length() > 1) {
                SetupButtons(state, type.split(";"), status);
            }
        }
        catch (JSONException e)
        {
            Toast.makeText(getApplicationContext(), "Ошибка при получении данных. Проверьте сетевые настройки.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    // Запуск анимации
    public  void StartAnimation() {
        menu.setVisibility(View.VISIBLE);
        background.animate().translationY(-2700).setDuration(800).setStartDelay(300);
        accessory.animate().translationX(200).setDuration(800).setStartDelay(450);
        menu.startAnimation(fromBottom);
    }


    // Включение/Отключение кнопок
    public void SetupButtons(Integer state, String[] types, String status) {

        if(types.length == 4)
        {
            radioGroup.setOnCheckedChangeListener (null);
            for(int i = 0; i < radioGroup.getChildCount(); i++){
                (radioGroup.getChildAt(i)).setEnabled(state == 3);
                ((RadioButton)radioGroup.getChildAt(i)).setText(types[i]);
                if(status.equals(types[i])) {
                    ((RadioButton)radioGroup.getChildAt(i)).setChecked(true);
                }
            }
            radioGroup.setOnCheckedChangeListener (change_Status);
        }

        switch (state)
        {
            case 1:
                sendAllButton.setEnabled(false);
                sendNullButton.setEnabled(true);
                sendCustomButton.setEnabled(true);
                break;
            case 2:
                sendAllButton.setEnabled(true);
                sendNullButton.setEnabled(false);
                sendCustomButton.setEnabled(true);
                break;
            case 3:
                sendAllButton.setEnabled(true);
                sendNullButton.setEnabled(true);
                sendCustomButton.setEnabled(false);
                break;
        }


    }

    @Override
    public void callBack(String result) {

        // Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();

        if(result.indexOf('{') == -1 && result.indexOf('}') == -1) {
            return;
        }

        if(METHOD.equals("GET")) {
            SetState(result);
        } else {
            UpdateState(result);
        }
    }

    @Override
    public void onUpdate(Integer... values) {
        switch (values[0])
        {
            // Начало передачи данных
            case 0:
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                break;

            // Конец передачи данных
            case 1:
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                break;

            // Ошибка авторизации
            case -2:
                if(IsFirst) StartAnimation();
                menu.setVisibility(View.INVISIBLE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                Toast.makeText(getApplicationContext(), "Не верный пароль! Проверьте настройки.", Toast.LENGTH_SHORT).show();
                break;


            case -1:
                StartAnimation();
                menu.setVisibility(View.INVISIBLE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                Toast.makeText(getApplicationContext(), "Произошла ошибка. Проверьте подключение!", Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == SettingsActivity.RESULT_OK) {
            METHOD = "GET";
            HttpAsync GetAsync = new HttpAsync(callBack, update, getApplicationContext());
            GetAsync.execute("Catalog_КонтурEDI_ТочкиДоставкиWEB?&$filter=Code eq '000000001'&$format=json");
        }
    }

    private RadioGroup.OnCheckedChangeListener change_Status = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            RadioButton radio = findViewById(checkedId);
            METHOD = "PATCH";
            String url = "Catalog_КонтурEDI_ТочкиДоставкиWEB(guid:'" + currentGuid + "')?$format=json";
            String data = "{\"Статус\":"+radio.getText().toString()+"}";

            HttpAsync PatchAsync = new HttpAsync(callBack, update, getApplicationContext());
            PatchAsync.execute(url, data);
        }
    };


    View.OnClickListener change_State = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(currentGuid.length() == 0) {

                METHOD = "GET";
                Toast.makeText(getApplicationContext(), "Ошибка", Toast.LENGTH_SHORT).show();
                HttpAsync GetAsync = new HttpAsync(callBack, update, getApplicationContext());
                GetAsync.execute("Catalog_КонтурEDI_ТочкиДоставкиWEB?&$filter=Code eq '000000001'&$format=json");
                return;
            }

            METHOD = "PATCH";

            String url = "Catalog_КонтурEDI_ТочкиДоставкиWEB(guid:'" + currentGuid + "')?$format=json";
            String key = " ";
            int value = 0;

            if(v == sendAllButton) {
                key = "Направление";
                value = 1;
            }else if(v == sendNullButton) {
                key = "Направление";
                value = 2;
            }else if(v == sendCustomButton) {
                key = "Направление";
                value = 3;
            }


            JSONObject data = new JSONObject();
            try {
                data.put(key, value);
            } catch (JSONException e) {
                e.printStackTrace();
                return;
            }

            HttpAsync PatchAsync = new HttpAsync(callBack, update, getApplicationContext());
            PatchAsync.execute(url, data.toString());

        }
    };


    private  void CreateAlertDialog(String title, String message) {
        builder.setMessage(message)
                .setTitle(title)
                .setCancelable(false);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

}
